package com.example.goa_project1;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.TextViewCompat;

import static androidx.core.widget.TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration;


public class MainActivity extends AppCompatActivity {

    String displayString = "0";
    String currentNumber = "0";
    double previousNumber = 0;
    String previousOperator = null;
    String previousDisplayString ="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TextView display = findViewById(R.id.display);
        TextView history = findViewById(R.id.history);

        //change page rip probably won't get to implement
        ImageButton flip = findViewById(R.id.flip);

        //operator buttons
        Button allClear = findViewById(R.id.allClear);
        Button percent = findViewById(R.id.percent);
        Button multiply = findViewById(R.id.multiply);
        Button divide = findViewById(R.id.divide);
        Button minus = findViewById(R.id.minus);
        Button add = findViewById(R.id.add);
        Button equals = findViewById(R.id.equals);
        Button decimal = findViewById(R.id.decimal);
        ImageButton backspace = findViewById(R.id.backspace);

        //number buttons
        Button nine = findViewById(R.id.nine);
        Button eight = findViewById(R.id.eight);
        Button seven = findViewById(R.id.seven);
        Button six = findViewById(R.id.six);
        Button five = findViewById(R.id.five);
        Button four = findViewById(R.id.four);
        Button three = findViewById(R.id.three);
        Button two = findViewById(R.id.two);
        Button one = findViewById(R.id.one);
        Button zero = findViewById(R.id.zero);



        allClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                displayString = "0";
                currentNumber = "0";
                previousNumber = 0;
                previousOperator = null;
                display.setText(displayString);
            }
        });

        percent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                char lastChar='`';
                if(displayString.length()>0){
                    lastChar = displayString.charAt(displayString.length()-1);
                }
                if(lastChar!='%') {
                    final String enteredDigit = "%";
                    if (currentNumber.equals("0")) {
                        // If display is "0", then replace it with the digit
                        displayString = enteredDigit;
                        currentNumber = enteredDigit;
                    } else {
                        // If display is not "0", then append enteredDigit to whatever number is displayed
                        displayString += enteredDigit;
                        currentNumber = Double.toString(Double.parseDouble(currentNumber) * 0.01);
                    }
                    display.setText(displayString);
                }
            }
        });

        multiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            /*if pending operator exists:
                Perform pending operation on previous number and current number
                Replace current number with result
                Save result as previous number
                Display current number

                Remember current number (as previous number)
                Remember selected operator (as pending operation)
                Set flag to clear display on next digit input
                */
                if(displayString.length()!=0) {
                    if (previousOperator != null) {
                        if (previousOperator.equals("*")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) * previousNumber);
                        } else if (previousOperator.equals("/")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) / previousNumber);
                        } else if (previousOperator.equals("-")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) - previousNumber);
                        } else if (previousOperator.equals("+")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) + previousNumber);
                        }
                    }

                    previousNumber = Double.parseDouble(currentNumber);
                    currentNumber = "0";
                    displayString += "×";
                    previousOperator = "*";
                    display.setText(displayString);
                }
            }
        });

        divide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(displayString.length()!=0) {
                    if (previousOperator != null) {
                        if (previousOperator.equals("*")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) * previousNumber);
                        } else if (previousOperator.equals("/")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) / previousNumber);
                        } else if (previousOperator.equals("-")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) - previousNumber);
                        } else if (previousOperator.equals("+")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) + previousNumber);
                        }
                    }

                    previousNumber = Double.parseDouble(currentNumber);
                    currentNumber = "0";
                    displayString += "÷";
                    previousOperator = "/";
                    display.setText(displayString);
                }
            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(displayString.length()!=0) {
                    if (previousOperator != null) {
                        if (previousOperator.equals("*")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) * previousNumber);
                        } else if (previousOperator.equals("/")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) / previousNumber);
                        } else if (previousOperator.equals("-")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) - previousNumber);
                        } else if (previousOperator.equals("+")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) + previousNumber);
                        }
                    }

                    previousNumber = Double.parseDouble(currentNumber);
                    currentNumber = "0";
                    displayString += "-";
                    previousOperator = "-";
                    display.setText(displayString);
                }
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(displayString.length()!=0) {
                    if (previousOperator != null) {
                        if (previousOperator.equals("*")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) * previousNumber);
                        } else if (previousOperator.equals("/")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) / previousNumber);
                        } else if (previousOperator.equals("-")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) - previousNumber);
                        } else if (previousOperator.equals("+")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) + previousNumber);
                        }
                    }

                    previousNumber = Double.parseDouble(currentNumber);
                    currentNumber = "0";
                    displayString += "+";
                    previousOperator = "+";
                    display.setText(displayString);
                }
            }
        });

        equals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(displayString.length()!=0) {
                    if (previousOperator != null) {
                        if (previousOperator.equals("*")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) * previousNumber);
                        } else if (previousOperator.equals("/")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) / previousNumber);
                        } else if (previousOperator.equals("-")) {
                            currentNumber = Double.toString((Double.parseDouble(currentNumber) - previousNumber)*-1);

                        } else if (previousOperator.equals("+")) {
                            currentNumber = Double.toString(Double.parseDouble(currentNumber) + previousNumber);
                        }
                    }
                    previousDisplayString = displayString;
                    displayString = currentNumber;
                    currentNumber = "0";
                    previousNumber = 0;
                    String previousOperator = null;
                    display.setText(displayString);
                    history.setText(previousDisplayString);
                }

            }
        });

        decimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String enteredDigit = ".";
                char lastChar='`';
                if(displayString.length()>0){
                    lastChar = displayString.charAt(displayString.length()-1);
                }
                if(lastChar!='.'&&lastChar!='%') {

                    // If display is not "%", then append enteredDigit to whatever number is displayed
                    displayString += enteredDigit;
                    currentNumber += enteredDigit;
                }


                display.setText(displayString);
            }
        });

        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(displayString.length()!=0) {
                    char lastChar = displayString.charAt(displayString.length() - 1);
                    displayString = displayString.substring(0, displayString.length() - 1);

                    if(lastChar=='%'&&currentNumber!="") {
                        currentNumber = Double.toString(Double.parseDouble(currentNumber) * 100);
                    }else if(lastChar=='%'){
                        previousNumber*=100;
                    }
                    else if (!currentNumber.equals("")) {
                        currentNumber = currentNumber.substring(0, currentNumber.length() - 1);
                    }

                    if (lastChar == '+' || lastChar == '-' || lastChar == '×' || lastChar == '÷') {
                        previousOperator = null;
                    }
                    display.setText(displayString);
                }
            }
        });

        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String enteredDigit = "9";
                char lastChar='`';
                if(displayString.length()>0){
                    lastChar = displayString.charAt(displayString.length()-1);
                }
                if (currentNumber.equals("0")&&lastChar=='0') {
                    // If display is "0", then replace it with the digit
                    displayString = enteredDigit;
                    currentNumber = enteredDigit;
                }
                else {
                    // If display is not "0", then append enteredDigit to whatever number is displayed
                    displayString += enteredDigit;
                    currentNumber += enteredDigit;
                }
                display.setText(displayString);
            }
        });

        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String enteredDigit = "8";
                char lastChar='`';
                if(displayString.length()>0){
                    lastChar = displayString.charAt(displayString.length()-1);
                }
                if (currentNumber.equals("0")&&lastChar=='0') {
                    // If display is "0", then replace it with the digit
                    displayString = enteredDigit;
                    currentNumber = enteredDigit;
                }
                else {
                    // If display is not "0", then append enteredDigit to whatever number is displayed
                    displayString += enteredDigit;
                    currentNumber += enteredDigit;
                }
                display.setText(displayString);
            }
        });

        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String enteredDigit = "7";
                char lastChar='`';
                if(displayString.length()>0){
                    lastChar = displayString.charAt(displayString.length()-1);
                }
                if (currentNumber.equals("0")&&lastChar=='0') {
                    // If display is "0", then replace it with the digit
                    displayString = enteredDigit;
                    currentNumber = enteredDigit;
                }
                else {
                    // If display is not "0", then append enteredDigit to whatever number is displayed
                    displayString += enteredDigit;
                    currentNumber += enteredDigit;
                }
                display.setText(displayString);
            }
        });

        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String enteredDigit = "6";
                char lastChar='`';
                if(displayString.length()>0){
                    lastChar = displayString.charAt(displayString.length()-1);
                }
                if (currentNumber.equals("0")&&lastChar=='0') {
                    // If display is "0", then replace it with the digit
                    displayString = enteredDigit;
                    currentNumber = enteredDigit;
                }
                else {
                    // If display is not "0", then append enteredDigit to whatever number is displayed
                    displayString += enteredDigit;
                    currentNumber += enteredDigit;
                }
                display.setText(displayString);
            }
        });

        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String enteredDigit = "5";
                char lastChar='`';
                if(displayString.length()>0){
                    lastChar = displayString.charAt(displayString.length()-1);
                }
                if (currentNumber.equals("0")&&lastChar=='0') {
                    // If display is "0", then replace it with the digit
                    displayString = enteredDigit;
                    currentNumber = enteredDigit;
                }
                else {
                    // If display is not "0", then append enteredDigit to whatever number is displayed
                    displayString += enteredDigit;
                    currentNumber += enteredDigit;
                }
                display.setText(displayString);
            }
        });

        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String enteredDigit = "4";
                char lastChar='`';
                if(displayString.length()>0){
                    lastChar = displayString.charAt(displayString.length()-1);
                }
                if (currentNumber.equals("0")&&lastChar=='0') {
                    // If display is "0", then replace it with the digit
                    displayString = enteredDigit;
                    currentNumber = enteredDigit;
                }
                else {
                    // If display is not "0", then append enteredDigit to whatever number is displayed
                    displayString += enteredDigit;
                    currentNumber += enteredDigit;
                }
                display.setText(displayString);
            }
        });

        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String enteredDigit = "3";
                char lastChar='`';
                if(displayString.length()>0){
                    lastChar = displayString.charAt(displayString.length()-1);
                }
                if (currentNumber.equals("0")&&lastChar=='0') {
                    // If display is "0", then replace it with the digit
                    displayString = enteredDigit;
                    currentNumber = enteredDigit;
                }
                else {
                    // If display is not "0", then append enteredDigit to whatever number is displayed
                    displayString += enteredDigit;
                    currentNumber += enteredDigit;
                }
                display.setText(displayString);
            }
        });

        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String enteredDigit = "2";
                char lastChar='`';
                if(displayString.length()>0){
                    lastChar = displayString.charAt(displayString.length()-1);
                }
                if (currentNumber.equals("0")&&lastChar=='0') {
                    // If display is "0", then replace it with the digit
                    displayString = enteredDigit;
                    currentNumber = enteredDigit;
                }
                else {
                    // If display is not "0", then append enteredDigit to whatever number is displayed
                    displayString += enteredDigit;
                    currentNumber += enteredDigit;
                }
                display.setText(displayString);
            }
        });

        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String enteredDigit = "1";
                char lastChar='`';
                if(displayString.length()>0){
                    lastChar = displayString.charAt(displayString.length()-1);
                }
                if (currentNumber.equals("0")&&lastChar=='0') {
                    // If display is "0", then replace it with the digit
                    displayString = enteredDigit;
                    currentNumber = enteredDigit;
                }
                else {
                    // If display is not "0", then append enteredDigit to whatever number is displayed
                    displayString += enteredDigit;
                    currentNumber += enteredDigit;
                }
                display.setText(displayString);
            }
        });

        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String enteredDigit = "0";
                char lastChar='`';
                if(displayString.length()>0){
                    lastChar = displayString.charAt(displayString.length()-1);
                }
                if (currentNumber.equals("0")&&lastChar=='0') {
                    // If display is "0", then replace it with the digit
                    displayString = enteredDigit;
                    currentNumber = enteredDigit;
                }
                else {
                    // If display is not "0", then append enteredDigit to whatever number is displayed
                    displayString += enteredDigit;
                    currentNumber += enteredDigit;
                }
                display.setText(displayString);
            }
        });

    }


}